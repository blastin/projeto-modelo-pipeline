run_newman() {
  echo "Iniciando testes automatizados"
  newman run collection.json -e environment.json --delay-request "${DELAY:=1}" --color on --verbose -n "${ITERATION_COUNT}" \
    -r cli,junit --reporter-junit-export '/tmp/junit'
}
# Verificar a situação de saúde do serviço
# Caso Retorne um status diferente de 200, ocorerrá uma nova tentativa
#
health_check() {

  status=0

  attempts=0

  while test "$status" -ne 200 -a "$attempts" -lt "${ATTEMPTS_HEALTH_CHECK}"; do
    echo "attempts : [$((ATTEMPTS_HEALTH_CHECK - attempts))], status : [$status] health check ..."
    status="$(curl -s -o /dev/null -w "%{http_code}" -k "${HEALTH_CHECK}")"
    sleep "${SLEEP_TIME}"
    attempts=$((attempts + 1))
  done

  if [ "$attempts" -lt "${ATTEMPTS_HEALTH_CHECK}" ]; then
    return 0
  fi

  return 1

}
#  Verificando se o comando curl foi instalado no sistema
#
if test -z "$(command -v curl)"; then
  echo "comando curl não foi encontrado"
  exit 1
fi
#
#
echo "serviço de health check = ${HEALTH_CHECK}"
#
#
health_check
result="$?"
if test "$result" -eq 1; then
  echo "Não foi possível comunicar com projeto"
  exit 1
fi
#
# Verificando se o comando newman foi instalado no sistema
if test -z "$(command -v newman)"; then
  echo "comando newman não foi encontrado"
  exit 1
else
  run_newman
  newman_resultado="$?"
  exit $newman_resultado
fi
