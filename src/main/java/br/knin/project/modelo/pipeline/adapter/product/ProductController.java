package br.knin.project.modelo.pipeline.adapter.product;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("produtos")
public class ProductController {

    @GetMapping
    public List<String> todos() {
        return List.of("A", "B");
    }
}
