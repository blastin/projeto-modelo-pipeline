package br.knin.project.modelo.pipeline.adapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModeloPipelineApplication {

    public static void main(String[] args) {
        SpringApplication.run(ModeloPipelineApplication.class, args);
    }

}
