FROM eclipse-temurin:16-alpine

RUN adduser service -D

USER service

WORKDIR /home/service

COPY target/*.jar projeto.jar

CMD ["java","-jar", "-Dspring.profiles.active=${ENVIRONMENT_PROFILE}","projeto.jar"]
